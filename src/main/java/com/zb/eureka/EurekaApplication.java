package com.zb.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author zhoubin0616
 * @title: SpringCloud单机版微服务注册中心
 * @projectName eureka
 * @description: SpringCloud单机版微服务注册中心
 * @date 2021/8/1715:25
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {

    /**
     * @description: 注册中心启动主入口
     * @param ${tags}
     * @return ${return_type}
     * @throws
     * @date 2021/8/17 15:34
     */
    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class, args);
    }

}
