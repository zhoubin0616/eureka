FROM java:8
ADD ./target/eureka-0.0.1-SNAPSHOT.jar eureka.jar
RUN bash -c 'touch /eureka.jar'
ENTRYPOINT ["java","-jar","/eureka.jar"]